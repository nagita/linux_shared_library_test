# Makefile

CC ?= gcc
CFLAGS = -fPIC -Wall -Wextra -O2 -g
LDFLAGS = -shared
RM = rm -f
TARGET_LIB = libfoo.so

TEST_EXE = test
TEST_EXE_2 = test2

SRCS = main.c foo.c
OBJS = $(SRCS:.c=.o)

.PHONY: all
all: $(TARGET_LIB) $(TEST_EXE) $(TEST_EXE_2)

$(TARGET_LIB): $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $^
	
$(TEST_EXE):
	$(CC) test_dll.c -ldl -o $(TEST_EXE) -lrt -lpthread

$(TEST_EXE_2):
	$(CC) test_dll_2.c -ldl -o $(TEST_EXE_2) -lrt -lpthread
	
$(SRCS:.c=.d):%.d:%.c
	$(CC) $(CFLAGS) -MM $< >$@
	
include $(SRCS:.c=.d)

.PHONY: clean
clean:
	-$(RM) ${TARGET_LIB} ${TEST_EXE} ${TEST_EXE_2} ${OBJS} $(SRCS:.c=.d)

#gcc -g -fPIC -c foo.c
#gcc -shared foo.o -o libfoo.so
#gcc -g -o main main.c libfoo.so
#gcc-g -o main main.c -lfoo -L.
