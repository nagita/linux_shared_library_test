#ifndef __FOO_H__
#define __FOO_H__


struct shared_data{
	int data1;
	char buf[100];
	int num;
};
 
extern struct shared_data *global;

void foo();
int counter_up();
int counter_down();

#endif // __FOO_H__
