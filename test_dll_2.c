#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <fcntl.h>       /* For O_* constants */
#include <sys/mman.h>
#include <unistd.h>
#include <semaphore.h>
int main(int argc, char * argv[]) {
	void * handle;
	void (*f)();
	int (*f_counter_up)();
	

	char * error;
	
	handle = dlopen("./libfoo.so", RTLD_LAZY);
	if (!handle) {
		fputs(dlerror(), stderr);
		exit(-1);
	}
	
	*(void**)(&f) = dlsym(handle, "foo");
	*(void**)(&f_counter_up) = dlsym(handle, "counter_up");
	
	sem_t * sem_mutex = sem_open("posix_semaphore", O_RDWR | O_CREAT, 0600, 1);
	
	printf("d2: wait semaphore\n");
	sem_wait(sem_mutex);
	printf("d2: in critical section\n");
	int fd = shm_open("posix_shared_memory", O_RDWR, 0666);
	ftruncate(fd, (4 * 1024 * 1024));
	
	char * p = mmap(NULL, (4 * 1024 * 1024), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	printf("%c %c %c %c %c %c %c %c\n", p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7]);
	
	munmap(p, (4 * 1024 * 1024));
	
	sem_post(sem_mutex);
	printf("d2: exit critical section\n");
	f();
	int count = f_counter_up();
	count = f_counter_up();
	count = f_counter_up();
	printf("counter: %d\n", count);
	
	
	dlclose(handle);
	fflush(stdout);
	return 0;
}
