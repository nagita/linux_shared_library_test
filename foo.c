#include "foo.h"

#include <stdio.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>       /* For O_* constants */
#include <string.h>
#include <semaphore.h>
// https://devarea.com/linux-shared-libraries/#.YOM_l-j7RmM
// shm_open and mmap

static void __attribute__ ((constructor)) init_lib(void);
static void __attribute__ ((destructor)) deinit_lib(void);

#define ALLOCATE_SHARED_MEMORY_SIZE (4 * 1024 * 1024)

char * shared_memory_address = NULL;


void init_lib(void)
{
	//int fd = shm_open("shared_struct_data", O_CREAT | O_RDWR, 0660);
	//ftruncate(fd, sizeof(struct shared_data));
	//global_data = mmap(0, sizeof(struct shared_data), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	printf("%s(%d): in\n", __func__, __LINE__);
	fflush(stdout);
	int fd2 = shm_open("posix_shared_memory", O_CREAT | O_RDWR | O_EXCL, 0666);
	printf("%s(%d): here: fd2(%d)\n", __func__, __LINE__, fd2);
	fflush(stdout);	
	if (fd2 >= 0) {
		printf("%s(%d): here\n", __func__, __LINE__);
		fflush(stdout);	
		ftruncate(fd2, ALLOCATE_SHARED_MEMORY_SIZE);
		shared_memory_address = mmap(NULL, ALLOCATE_SHARED_MEMORY_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd2, 0);
		printf("%s(%d): here: %p\n", __func__, __LINE__, shared_memory_address);
		fflush(stdout);	
		msync(shared_memory_address, ALLOCATE_SHARED_MEMORY_SIZE, MS_SYNC | MS_INVALIDATE);	
		memset(shared_memory_address, 'A', ALLOCATE_SHARED_MEMORY_SIZE);
		printf("%s(%d): here\n", __func__, __LINE__);
		fflush(stdout);		
		//munmap(p, 0x40000);
	}
	
	
	sem_t * semaphore_mutex = NULL;
	semaphore_mutex = sem_open("posix_semaphore", O_RDWR | O_CREAT, 0600, 1);
	
	
	printf("%s(%d): out\n", __func__, __LINE__);
	fflush(stdout);
}

void deinit_lib(void)
{
	printf("%s(%d): in\n", __func__, __LINE__);
	if (shared_memory_address) {
		munmap(shared_memory_address, ALLOCATE_SHARED_MEMORY_SIZE);
	}
	printf("%s(%d): out\n", __func__, __LINE__);
	fflush(stdout);
}


static int counter = 0;

void foo() {
	printf("call foo\n");
}

int counter_up() {
	return counter++;
}

int counter_down() {
	return counter--;
}

