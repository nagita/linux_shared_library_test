#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <fcntl.h>       /* For O_* constants */
#include <sys/mman.h>
#include <unistd.h>
#include <string.h>
#include <semaphore.h>
int main(int argc, char * argv[]) {
	void * handle;
	void (*f)();
	int (*f_counter_up)();
	

	char * error;
	
	handle = dlopen("./libfoo.so", RTLD_LAZY);
	if (!handle) {
		fputs(dlerror(), stderr);
		exit(-1);
	}
	
	*(void**)(&f) = dlsym(handle, "foo");
	*(void**)(&f_counter_up) = dlsym(handle, "counter_up");
	
	sem_t * sem_mutex = sem_open("posix_semaphore", O_RDWR | O_CREAT, 0600, 1);
	
	sem_wait(sem_mutex);
	
	
	int fd = shm_open("posix_shared_memory", O_RDWR, 0666);
	ftruncate(fd, (4 * 1024 * 1024));
	char * p = mmap(NULL, (4 * 1024 * 1024), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	printf("%c %c %c %c\n", p[0], p[1], p[2], p[3]);
	
	memset(p + 4, 'B', sizeof(char) * 4);
	
	munmap(p, (4 * 1024 * 1024));
	
	sleep(20);
	sem_post(sem_mutex);
	
	f();
	int count = f_counter_up();
	count = f_counter_up();
	printf("counter: %d\n", count);
	
	
	dlclose(handle);
	fflush(stdout);
	return 0;
}
